class Deck {
    // index 0 is the card on top of the deck when faced down
    constructor(solitaire) {
        this.solitaire = solitaire;
        this.container = document.createElement("div");
        this.container.classList.add("deck");
        this.cards = [];
        for(let suit in solitaire.SUIT) {
            for(let i = 1; i <= 13; i++) {
                const card = new Card(solitaire, solitaire.SUIT[suit], i);
                this.cards.push(card);
            }
        }
        this.container.addEventListener("click", e => {
            if(e.target.className !== "back")
                this.solitaire.remainingCards.cardSelection.returnToDeck();
        });
    }
    drawCards() {
        let cards = [];
        for(let i = 0; i < 3; i++) {
            if(this.cards.length !== 0)
                cards.push(this.drawCard());
        }
        this.solitaire.remainingCards.cardSelection.addCards(cards);
    }
    renderCards() {
        this.container.innerHTML = "";
        for(let i in this.cards) {
            const card = this.cards[i];
            this.container.insertAdjacentElement("beforeend", card.container);
        }
    }
    shuffle() {
        for (let i = this.cards.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [this.cards[i], this.cards[j]] = [this.cards[j], this.cards[i]];
        }
        this.renderCards();
    }
    drawCard() {
        return this.cards.shift();
    }
    returnCard(card) {
        this.container.appendChild(card.container)
        card.setSlot(null);
        this.cards.push(card);
        card.flip();
    }
}