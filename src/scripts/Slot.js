class Slot {
    constructor(solitaire) {
        this.container = document.createElement("div");
        this.container.classList.add("slot");
        this.cards = [];

        this.container.addEventListener("click", e => {
            if(e.target.className !== "front" && e.target.className !== "back") {
                console.log(solitaire)
                if(solitaire.selectedCard.value === 13) {
                    solitaire.selectedCard.slot.moveCards(solitaire.selectedCard, this);
                }
            }
        })
    }
    placeCard(card, visible = false) {
        this.container.appendChild(card.container)
        card.offsetTop(this.cards.length);
        card.setSlot(this);
        this.cards.push(card);
        if(visible) card.flip();
    }
    flipLastCard() {
        if(this.cards.length === 0) return;
        this.cards[this.cards.length - 1].flip();
    }
    getLastCard() {
        if(this.cards.length === 0) return null;
        return this.cards[this.cards.length - 1];
    }
    getCardIndex(card) {
        for(let c in this.cards) {
            if(card.isEqual(this.cards[c])) return c;
        }
        return null;
    }
    moveCards(card, newSlot) {
        const cardIndex = this.getCardIndex(card);
        const cards = this.cards.slice(cardIndex);
        this.cards.splice(cardIndex);
        newSlot.placeCards(cards);
        if(this.getLastCard() !== null) {
            if(!this.getLastCard().visible)
                this.flipLastCard();
        }
    }
    removeLastCard() {
        this.cards.pop();
        const lastCard = this.getLastCard();
        if(!lastCard.visible) {
            lastCard.flip();
        }
    }
    placeCards(cards) {
        for(let c in cards) {
            this.placeCard(cards[c]);
        }
    }
}