class OrderedCards {
    constructor(solitaire) {
        this.container = document.createElement("div");
        this.container.classList.add("ordered-cards");
        this.data = {}
        for(let suit in solitaire.SUIT) {
            const suitString = solitaire.SUIT[suit];
            this.data[suitString] = {
                "container": document.createElement("div"),
                "cards": []
            }
            this.data[suitString].container.classList.add("suit-" + suitString);
            
            this.data[suitString].container.addEventListener("click", e => {
                if(this.canPlace(suitString, solitaire.selectedCard)) {
                    solitaire.selectedCard.slot.removeLastCard();
                    this.data[suitString].cards.push(solitaire.selectedCard);
                    this.data[suitString].container.appendChild(solitaire.selectedCard.container);
                    solitaire.selectedCard.setSlot(this);
                    solitaire.selectedCard.offsetTop(0);
                    solitaire.selectedCard.unselect();
                }
            });

            const suitIcon = document.createElement("i");
            suitIcon.classList.add("mdi", "mdi-cards-" + suitString + "-outline");
            this.data[suitString].container.insertAdjacentElement("beforeend", suitIcon);
            this.container.insertAdjacentElement("beforeend", this.data[suitString].container)
        }
    }
    getSuitLastValue(suit) {
        return this.data[suit].cards.length;
        
    }
    canPlace(suitString, card) {
        const suit = card.suit;
        const suitLastValue = this.getSuitLastValue(suit);
        return !(suitString !== card.suit || card.value !== suitLastValue + 1)
    }
}