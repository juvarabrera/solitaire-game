class CardSelection {
    constructor(solitaire) {
        this.solitaire = solitaire;
        this.container = document.createElement("div");
        this.container.classList.add("card-selection");
        this.cards = [];
    }
    addCards(cards) {
        for(let i = 0; i < cards.length; i++) {
            const card = cards[i];
            this.cards.push(card);
            this.container.appendChild(card.container);
            card.flip();
            card.setSlot(this);
        }
    }
    removeLastCard() {
        this.cards.pop();
        const lastCard = this.getLastCard();
        if(!lastCard.visible) {
            lastCard.flip();
        }
    }
    getLastCard() {
        if(this.cards.length === 0) return null;
        return this.cards[this.cards.length - 1];
    }
    moveCards(card, newSlot) {
        this.cards.pop();
        newSlot.placeCards([card]);
    }
    returnToDeck() {
        while(this.cards.length !== 0) {
            const card = this.cards.shift();
            this.solitaire.remainingCards.deck.returnCard(card);
        }
    }
}