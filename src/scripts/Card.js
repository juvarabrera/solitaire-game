class Card {
    FACE_CARDS = {
        "1": "A",
        "11": "J",
        "12": "Q",
        "13": "K"
    }
    constructor(solitaire, suit, value) {
        this.solitaire = solitaire;
        this.slot = null;
        this.suit = suit;
        this.value = value;
        this.visible = false;

        this.container = document.createElement("div");
        this.container.classList.add("card", "suit-" + this.suit);
        
        const inner = document.createElement("div");
        inner.classList.add("inner");
        this.container.insertAdjacentElement("beforeend", inner);

        const front = document.createElement("div");
        front.classList.add("front");
        const topSymbol = document.createElement("div");
        const suitIcon = document.createElement("i");
        suitIcon.classList.add("mdi", "mdi-cards-" + this.suit);
        const valueIcon = document.createElement("span");
        valueIcon.textContent = (Object.keys(this.FACE_CARDS).indexOf(this.value + "") === -1 ? value : this.FACE_CARDS[value]);
        topSymbol.insertAdjacentElement("beforeend", valueIcon);
        topSymbol.insertAdjacentElement("beforeend", suitIcon);
        front.insertAdjacentElement("beforeend", topSymbol);
        const centerSymbol = suitIcon.cloneNode();
        front.insertAdjacentElement("beforeend", centerSymbol);
        const bottomSymbol = topSymbol.cloneNode(true);
        front.insertAdjacentElement("beforeend", bottomSymbol);
        inner.insertAdjacentElement("beforeend", front);

        const back = document.createElement("div");
        back.classList.add("back");
        inner.insertAdjacentElement("beforeend", back);
        this.container.insertAdjacentElement("beforeend", inner);

        this.flipClass();

        this.container.addEventListener("click", e => {
            if(this.slot === null) {
                solitaire.remainingCards.deck.drawCards();
                if(solitaire.selectedCard !== null) {
                    solitaire.selectedCard.unselect();
                }
                return;
            }
            if(this.slot.constructor.name === "Slot") {
                if(solitaire.selectedCard === null) {
                    this.select();
                } else {
                    const lastCard = this.slot.getLastCard();
                    if(lastCard.canSelectedCardBePlaceHere()) {
                        solitaire.selectedCard.slot.moveCards(solitaire.selectedCard, this.slot); 
                    }
                    solitaire.selectedCard.unselect();
                }
            } else if(this.slot.constructor.name === "CardSelection") {
                if(solitaire.selectedCard === null) {
                    this.select();
                }
            }
        })
    }
    select() {
        this.container.classList.add("selected");
        solitaire.selectedCard = this;
    }
    unselect() {
        this.container.classList.remove("selected");
        this.solitaire.selectedCard = null;
    }
    getNextCardsInSlot() {
        let cards = [];
        if(slot !== null) {
            let selected = false;
            for(let card in this.slot.cards) {
                if(this.isEqual(card)) selected = true;
                if(selected) cards.push(card);
            }
        }
        return cards;
    }
    isEqual(card) {
        return this.value === card.value && this.suit === card.suit;
    }
    canSelectedCardBePlaceHere() {
        if(this.solitaire.selectedCard.value + 1 === this.value) {
            if(this.getOpposingSuit().indexOf(this.solitaire.selectedCard.suit) !== -1)
                return true;
        }
        return false;
    }
    /**
     * Returns suit in array depending on the suit of the current card.
     * 
     */
    getOpposingSuit() {
        if(this.suit === "spade" || this.suit === "club")
            return ["diamond", "heart"];
        return ["spade", "club"];
    }
    setSlot(slot) {
        this.slot = slot;
    }
    flip() {
        this.visible = !this.visible;
        this.flipClass();
    }
    flipClass() {
        this.container.classList.add("hidden");
        if(this.visible) this.container.classList.remove("hidden");
    }
    offsetTop(n) {
        this.container.style.top = (n * 25) + "px";
    }
}