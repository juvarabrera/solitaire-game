class RemainingCards {
    constructor(solitaire) {
        this.container = document.createElement("div");
        this.container.classList.add("remaining-cards");
        
        this.cardSelection = new CardSelection(solitaire);
        
        this.deck = new Deck(solitaire);

        this.container.insertAdjacentElement("beforeend", this.cardSelection.container);
        this.container.insertAdjacentElement("beforeend", this.deck.container);
    }
}